/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Slot,
    condition,
    tripetto,
} from "tripetto-runner-foundation";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:undefined`,
})
export class PictureChoiceUndefinedCondition extends ConditionBlock {
    @condition
    isUndefined(): boolean {
        const choiceSlot = this.valueOf<string>("choice");

        if (choiceSlot) {
            return choiceSlot.value ? false : true;
        } else {
            const slots = this.slots;

            return (
                !slots ||
                !slots.each((slot: Slot) => {
                    const choice = this.valueOf<boolean>(slot);

                    return (choice && choice.value) || false;
                })
            );
        }
    }
}
