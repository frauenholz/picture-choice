/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;

/** Dependencies */
import {
    ConditionBlock,
    Num,
    Slots,
    condition,
    isNumberFinite,
    isString,
    tripetto,
} from "tripetto-runner-foundation";

export type TCounterModes =
    | "equal"
    | "not-equal"
    | "below"
    | "above"
    | "between"
    | "not-between";

@tripetto({
    type: "condition",
    identifier: `${PACKAGE_NAME}:counter`,
})
export class CounterCondition extends ConditionBlock<{
    readonly mode: TCounterModes;
    readonly value?: number | string;
    readonly to?: number | string;
}> {
    private getValue(slot: Slots.Slot, value: number | string | undefined) {
        if (isString(value) && slot instanceof Slots.Number) {
            const variable = this.variableFor(value);

            return variable && variable.hasValue
                ? slot.toValue(variable.value)
                : undefined;
        }

        return isNumberFinite(value) ? value : undefined;
    }

    @condition
    verify(): boolean {
        const counterSlot = this.valueOf<number>();

        if (counterSlot) {
            const value = this.getValue(counterSlot.slot, this.props.value);

            switch (this.props.mode) {
                case "equal":
                    return (
                        (counterSlot.hasValue
                            ? counterSlot.value
                            : undefined) === value
                    );
                case "not-equal":
                    return (
                        (counterSlot.hasValue
                            ? counterSlot.value
                            : undefined) !== value
                    );
                case "below":
                    return (
                        isNumberFinite(value) &&
                        counterSlot.hasValue &&
                        counterSlot.value < value
                    );
                case "above":
                    return (
                        isNumberFinite(value) &&
                        counterSlot.hasValue &&
                        counterSlot.value > value
                    );
                case "between":
                case "not-between":
                    const to = this.getValue(counterSlot.slot, this.props.to);

                    return (
                        isNumberFinite(value) &&
                        isNumberFinite(to) &&
                        (counterSlot.hasValue &&
                            counterSlot.value >= Num.min(value, to) &&
                            counterSlot.value <= Num.max(value, to)) ===
                            (this.props.mode === "between")
                    );
            }
        }

        return false;
    }
}
