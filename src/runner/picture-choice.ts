import {
    NodeBlock,
    Slots,
    Str,
    Value,
    each,
    filter,
    findFirst,
    isNumberFinite,
    map,
    markdownifyToString,
    markdownifyToURL,
    reduce,
    validator,
} from "tripetto-runner-foundation";
import { IChoice, IPictureChoice } from "./interface";
import "./conditions/choice";

export abstract class PictureChoice extends NodeBlock<IPictureChoice> {
    /** Contains the randomized choices order. */
    private randomized?: {
        readonly index: number;
        readonly id: string;
    }[];

    /** Contains the counter slot. */
    readonly counterSlot = this.valueOf<number, Slots.Number>(
        "counter",
        "feature"
    );

    /** Contains the concatenation slot. */
    readonly concatenationSlot = this.valueOf<string, Slots.Text>(
        "concatenation",
        "feature"
    );

    /** Contains the score slot. */
    readonly scoreSlot = this.valueOf<number, Slots.Numeric>(
        "score",
        "feature"
    );

    /** Contains the single choice slot. */
    readonly singleChoiceSlot = this.valueOf<string, Slots.String>(
        "choice",
        "static",
        {
            confirm: true,
            modifier: (data) => {
                if (data.value) {
                    if (!data.reference) {
                        const selected =
                            findFirst(
                                this.props.choices,
                                (choice) => choice.value === data.value
                            ) ||
                            findFirst(
                                this.props.choices,
                                (choice) => choice.id === data.value
                            ) ||
                            findFirst(
                                this.props.choices,
                                (choice) => choice.name === data.value
                            ) ||
                            findFirst(
                                this.props.choices,
                                (choice) =>
                                    choice.name.toLowerCase() ===
                                    data.value.toLowerCase()
                            );

                        return {
                            value:
                                selected && (selected.value || selected.name),
                            reference: selected && selected.id,
                        };
                    } else if (
                        !findFirst(
                            this.props.choices,
                            (choice) => choice.id === data.reference
                        )
                    ) {
                        return {
                            value: undefined,
                            reference: undefined,
                        };
                    }
                }
            },
            onChange: (slot) => {
                if (this.scoreSlot) {
                    const selected = findFirst(
                        this.props.choices,
                        (choice) => choice.id === slot.reference
                    );

                    this.scoreSlot.set(selected && (selected?.score || 0));
                }
            },
        }
    );

    get required(): boolean {
        return this.props.required || false;
    }

    private transform(): void {
        if (this.props.multiple) {
            const choices = filter(
                map(this.props.choices, (choice) => ({
                    id: choice.id,
                    exclusive: choice.exclusive,
                    valueRef: this.choiceSlot(choice),
                })),
                (choice) => choice.valueRef?.value === true
            ).sort((a, b) => (b.valueRef?.time || 0) - (a.valueRef?.time || 0));
            const lastSelected = choices.length && choices[0];

            if (lastSelected) {
                choices.forEach((c) => {
                    if (
                        c.id !== lastSelected.id &&
                        (lastSelected.exclusive || c.exclusive)
                    ) {
                        c.valueRef!.value = false;
                    }
                });
            }

            if (this.props.max) {
                const max = this.props.max;
                let n = 0;

                choices.forEach((c) => {
                    if (c.valueRef?.value === true) {
                        n++;

                        if (n > max) {
                            c.valueRef!.value = false;
                        }
                    }
                });
            }

            if (this.counterSlot) {
                const n = filter(
                    choices,
                    (choice) => choice.valueRef?.value === true
                ).length;

                this.counterSlot.set(n);
            }

            if (this.concatenationSlot) {
                const list: string[] = [];
                let s = "";
                let n = 0;

                each(this.props.choices, (choice) => {
                    const label = choice.value || choice.name || "";

                    if (
                        label &&
                        this.valueOf<boolean>(choice.id)?.value === true
                    ) {
                        switch (this.props.formatSeparator) {
                            case "space":
                                s += (s === "" ? "" : " ") + label;
                                break;
                            case "list":
                                s += (s === "" ? "" : "\n") + label;
                                break;
                            case "bullets":
                                s += (s === "" ? "" : "\n") + "- " + label;
                                break;
                            case "numbers":
                                s +=
                                    (s === "" ? "" : "\n") + `${++n}. ${label}`;
                                break;
                            case "conjunction":
                            case "disjunction":
                                list.push(label);
                                break;
                            case "custom":
                                s +=
                                    (s === ""
                                        ? ""
                                        : this.props.formatSeparatorCustom ||
                                          "") + label;
                                break;
                            default:
                                s += (s === "" ? "" : ", ") + label;
                                break;
                        }
                    }
                });

                if (
                    this.props.formatSeparator === "conjunction" ||
                    this.props.formatSeparator === "disjunction"
                ) {
                    try {
                        const formatter = new Intl.ListFormat(
                            this.context.l10n.current || "en",
                            { type: this.props.formatSeparator }
                        );

                        s = formatter.format(list);
                    } catch {
                        s = Str.iterateToString(list, ", ");
                    }
                }

                this.concatenationSlot.set(s);
            }
        }
    }

    private score(choice: IChoice): void {
        if (this.scoreSlot && isNumberFinite(choice.score)) {
            this.scoreSlot.set(
                reduce(
                    this.props.choices,
                    (score, c) =>
                        score +
                        ((this.valueOf(c.id)?.value === true && c.score) || 0),
                    0
                )
            );
        }
    }

    /** Retrieves a choice slot. */
    choiceSlot(choice: IChoice): Value<boolean, Slots.Boolean> | undefined {
        return (
            (this.props.multiple &&
                this.valueOf<boolean>(choice.id, "dynamic", {
                    confirm: true,
                    onChange: () => {
                        this.transform();
                        this.score(choice);
                    },
                    onContext: (value, context) => {
                        if (this.scoreSlot) {
                            context
                                .contextualValueOf(this.scoreSlot)
                                ?.set(
                                    value.value === true
                                        ? choice.score || 0
                                        : undefined
                                );
                        }
                    },
                })) ||
            undefined
        );
    }

    /** Retrieves the choices. */
    choices<T>(props?: { readonly tabIndex: number }): (IChoice & {
        readonly slot?: Value<boolean, Slots.Boolean>;
        readonly tabIndex?: number;
    })[] {
        const n =
            (this.props.multiple &&
                this.props.max &&
                filter(
                    this.props.choices,
                    (choice) => this.choiceSlot(choice)?.value === true
                ).length) ||
            0;
        const choices =
            this.props.choices?.map((choice) => {
                const slot = this.choiceSlot(choice);

                return {
                    ...choice,
                    name: markdownifyToString(choice.name, this.context),
                    image:
                        (choice.image &&
                            markdownifyToURL(
                                choice.image,
                                this.context,
                                undefined,
                                [
                                    "image/jpeg",
                                    "image/png",
                                    "image/svg",
                                    "image/gif",
                                ]
                            )) ||
                        undefined,
                    description:
                        (choice.description &&
                            markdownifyToString(
                                choice.description,
                                this.context
                            )) ||
                        undefined,
                    url:
                        (choice.url &&
                            markdownifyToURL(choice.url, this.context)) ||
                        undefined,
                    slot,
                    tabIndex: props?.tabIndex,
                    disabled:
                        (this.props.max &&
                            n >= this.props.max &&
                            slot?.value !== true) ||
                        false,
                };
            }) || [];

        if (this.props.randomize && choices.length > 1) {
            if (
                !this.randomized ||
                this.randomized.length !== choices.length ||
                findFirst(
                    this.randomized,
                    (choice) => choices[choice.index]?.id !== choice.id
                )
            ) {
                this.randomized = choices.map((choice, index) => ({
                    index,
                    id: choice.id,
                }));

                let length = this.randomized.length;

                while (--length) {
                    const index = Math.floor(Math.random() * length);
                    const temp = this.randomized[length];

                    this.randomized[length] = this.randomized[index];
                    this.randomized[index] = temp;
                }
            }

            return this.randomized.map((choice) => choices[choice.index]);
        } else if (this.randomized) {
            this.randomized = undefined;
        }

        return choices;
    }

    @validator
    validate(): boolean {
        if (this.props.multiple && (this.props.min || this.props.max)) {
            const n = filter(
                this.props.choices,
                (choice) => this.choiceSlot(choice)?.value === true
            ).length;

            if (
                (this.props.min && n < this.props.min) ||
                (this.props.max && n > this.props.max)
            ) {
                return false;
            }
        }

        if (this.props.required) {
            if (this.props.multiple) {
                return findFirst(
                    this.props.choices,
                    (choice) => this.choiceSlot(choice)?.value === true
                )
                    ? true
                    : false;
            } else {
                return this.singleChoiceSlot?.hasValue || false;
            }
        }

        return true;
    }
}

// As long as the typings for Intl are incomplete, we need to declare them ourselves.
// See https://github.com/microsoft/TypeScript/issues/46907
declare namespace Intl {
    class ListFormat {
        constructor(
            locales?: string | string[],
            options?: {
                type?: "conjunction" | "disjunction";
            }
        );
        format(values: string[]): string;
    }
}
