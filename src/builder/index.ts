/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Dependencies */
import {
    Collection,
    Forms,
    NodeBlock,
    REGEX_IS_URL,
    Slots,
    affects,
    conditions,
    definition,
    each,
    editor,
    insertVariable,
    isBoolean,
    isNumber,
    isString,
    npgettext,
    pgettext,
    slots,
    supplies,
    tripetto,
} from "tripetto";
import { Choice } from "./choice";
import { PictureChoiceCondition } from "./conditions/choice";
import { PictureChoiceUndefinedCondition } from "./conditions/undefined";
import { ScoreCondition } from "./conditions/score";
import { TScoreModes } from "../runner/conditions/score";
import { CounterCondition } from "./conditions/counter";
import { TCounterModes } from "../runner/conditions/counter";

/** Assets */
import ICON from "../../assets/icon.svg";
import ICON_SINGLE from "../../assets/single.svg";
import ICON_MULTIPLE from "../../assets/multiple.svg";
import ICON_COUNTER from "../../assets/counter.svg";
import ICON_SCORE from "../../assets/score.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    icon: ICON,
    alias: "picture-choice",
    get label() {
        return pgettext("block:picture-choice", "Picture choice");
    },
})
export class PictureChoice extends NodeBlock {
    @definition("string", "optional")
    caption?: string;

    @definition("string", "optional")
    imageURL?: string;

    @definition("string", "optional")
    imageWidth?: string;

    @definition("boolean", "optional")
    imageAboveText?: boolean;

    @definition("items")
    @affects("#name")
    @supplies<PictureChoice>("#slot", "choice")
    readonly choices = Collection.of<Choice, PictureChoice>(Choice, this);

    @definition("boolean", "optional")
    @affects("#slots")
    @affects("#collection", "choices")
    multiple?: boolean;

    @definition("number", "optional")
    min?: number;

    @definition("number", "optional")
    max?: number;

    @definition("boolean", "optional")
    @affects("#required")
    @affects("#slots")
    @affects("#collection", "choices")
    required?: boolean;

    @definition("string", "optional")
    @affects("#slots")
    @affects("#collection", "choices")
    @affects("#label")
    alias?: string;

    @definition("boolean", "optional")
    @affects("#slots")
    @affects("#collection", "choices")
    exportable?: boolean;

    @definition("string", "optional")
    @affects("#collection", "choices")
    labelForTrue?: string;

    @definition("string", "optional")
    @affects("#collection", "choices")
    labelForFalse?: string;

    @definition("boolean", "optional")
    randomize?: boolean;

    @definition
    size?: "small" | "medium" | "large";

    @definition("string", "optional")
    @affects("#slots")
    @affects("#collection", "choices")
    format?: "fields" | "concatenate" | "both";

    @definition("string", "optional")
    formatSeparator?:
        | "comma"
        | "space"
        | "list"
        | "bullets"
        | "numbers"
        | "conjunction"
        | "disjunction"
        | "custom";

    @definition("string", "optional")
    formatSeparatorCustom?: string;

    get label() {
        return npgettext(
            "block:picture-choice",
            "%2 (%1 image)",
            "%2 (%1 images)",
            this.choices.count,
            this.type.label
        );
    }

    @slots
    defineSlot(): void {
        if (this.multiple) {
            this.slots.delete("choice", "static");

            this.slots.feature({
                type: Slots.Number,
                reference: "counter",
                label: pgettext("block:picture-choice", "Counter"),
                exportable: false,
            });

            if (this.format === "concatenate" || this.format === "both") {
                this.slots.feature({
                    type: Slots.Text,
                    reference: "concatenation",
                    label: pgettext("block:picture-choice", "Text value"),
                    exportable: this.exportable,
                    alias: this.alias,
                });
            } else {
                this.slots.delete("concatenation", "feature");
            }
        } else {
            this.slots.delete("counter", "feature");
            this.slots.delete("concatenation", "feature");

            this.slots.static({
                type: Slots.String,
                reference: "choice",
                label: pgettext("block:picture-choice", "Image"),
                alias: this.alias,
                required: this.required,
                exportable: this.exportable,
                exchange: ["alias", "required", "exportable"],
            });
        }
    }

    @editor
    defineEditor(): void {
        this.editor.name(true, true);
        this.editor.option({
            name: pgettext("block:picture-choice", "Caption"),
            form: {
                title: pgettext("block:picture-choice", "Caption"),
                controls: [
                    new Forms.Text(
                        "multiline",
                        Forms.Text.bind(this, "caption", undefined)
                    )
                        .placeholder(
                            pgettext(
                                "block:picture-choice",
                                "Type caption text here..."
                            )
                        )
                        .action("@", insertVariable(this)),
                ],
            },
            activated: isString(this.caption),
        });
        this.editor.description();
        this.editor.option({
            name: pgettext("block:picture-choice", "Image"),
            form: {
                title: pgettext("block:picture-choice", "Image"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "imageURL", undefined)
                    )
                        .label(
                            pgettext("block:picture-choice", "Image source URL")
                        )
                        .inputMode("url")
                        .placeholder("https://")
                        .action("@", insertVariable(this))
                        .autoValidate((ref: Forms.Text) =>
                            ref.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(ref.value) ||
                                  (ref.value.length > 23 &&
                                      ref.value.indexOf(
                                          "data:image/jpeg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/png;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/svg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/gif;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 1 &&
                                      ref.value.charAt(0) === "/")
                                ? "pass"
                                : "fail"
                        ),
                    new Forms.Text(
                        "singleline",
                        Forms.Checkbox.bind(this, "imageWidth", undefined)
                    )
                        .label(
                            pgettext(
                                "block:picture-choice",
                                "Image width (optional)"
                            )
                        )
                        .width(100)
                        .align("center"),
                    new Forms.Checkbox(
                        pgettext(
                            "block:picture-choice",
                            "Display image on top of the paragraph"
                        ),
                        Forms.Checkbox.bind(this, "imageAboveText", undefined)
                    ),
                ],
            },
            activated: isString(this.imageURL),
        });
        this.editor.explanation();

        const collection = this.editor.collection({
            collection: this.choices,
            title: pgettext("block:picture-choice", "Images"),
            placeholder: pgettext("block:picture-choice", "Unnamed image"),
            icon: this.multiple ? ICON_MULTIPLE : ICON_SINGLE,
            autoOpen: true,
            allowVariables: true,
            allowImport: true,
            allowExport: true,
            allowDedupe: true,
            showAliases: true,
            sorting: "manual",
            indicator: (choice) =>
                (this.multiple &&
                    choice.exclusive &&
                    pgettext(
                        "block:picture-choice",
                        "Exclusive"
                    ).toUpperCase()) ||
                undefined,
            emptyMessage: pgettext(
                "block:picture-choice",
                "Click the + button to add an image..."
            ),
        });

        this.editor.groups.settings();
        this.editor.option({
            name: pgettext("block:picture-choice", "Multiple select"),
            form: {
                title: pgettext("block:picture-choice", "Multiple select"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:picture-choice",
                            "Allow the selection of multiple images"
                        ),
                        Forms.Checkbox.bind(this, "multiple", undefined, true)
                    ).on(() => {
                        limitsFeature.disabled(!this.multiple);
                        labelsFeature.disabled(!this.multiple);
                        formatFeature.disabled(!this.multiple);

                        collection.icon = this.multiple
                            ? ICON_MULTIPLE
                            : ICON_SINGLE;

                        this.choices.each((choice) => {
                            if (choice.exclusive === true) {
                                choice.refresh("name");
                            }
                        });
                    }),
                ],
            },
            activated: isBoolean(this.multiple),
        });

        const min = new Forms.Numeric(
            Forms.Numeric.bind(this, "min", undefined)
        )
            .min(1)
            .max(this.max)
            .visible(isNumber(this.min))
            .indent(32)
            .width(75)
            .on(() => {
                max.min(this.min || 1);
            });
        const max = new Forms.Numeric(
            Forms.Numeric.bind(this, "max", undefined)
        )
            .min(this.min || 1)
            .visible(isNumber(this.max))
            .indent(32)
            .width(75)
            .on(() => {
                min.max(this.max);
            });

        const limitsFeature = this.editor.option({
            name: pgettext("block:picture-choice", "Limits"),
            form: {
                title: pgettext("block:picture-choice", "Limits"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:picture-choice",
                            "Minimum number of selected images"
                        ),
                        isNumber(this.min)
                    ).on((c) => {
                        min.visible(c.isChecked);
                    }),
                    min,
                    new Forms.Checkbox(
                        pgettext(
                            "block:picture-choice",
                            "Maximum number of selected images"
                        ),
                        isNumber(this.max)
                    ).on((c) => {
                        max.visible(c.isChecked);
                    }),
                    max,
                ],
            },
            activated: isNumber(this.max) || isNumber(this.min),
            disabled: !this.multiple,
        });

        this.editor.option({
            name: pgettext("block:picture-choice", "Randomization"),
            form: {
                title: pgettext("block:picture-choice", "Randomization"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:picture-choice",
                            "Randomize the images (using [Fisher-Yates shuffle](%1))",
                            "https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle"
                        ),
                        Forms.Checkbox.bind(this, "randomize", undefined, true)
                    ).markdown(),
                ],
            },
            activated: isBoolean(this.randomize),
        });

        this.editor.option({
            name: pgettext("block:picture-choice", "Size"),
            form: {
                title: pgettext("block:picture-choice", "Size"),
                controls: [
                    new Forms.Radiobutton<"small" | "medium" | "large">(
                        [
                            {
                                label: pgettext(
                                    "block:picture-choice",
                                    "Small"
                                ),
                                value: "small",
                            },
                            {
                                label: pgettext(
                                    "block:picture-choice",
                                    "Medium"
                                ),
                                value: "medium",
                            },
                            {
                                label: pgettext(
                                    "block:picture-choice",
                                    "Large"
                                ),
                                value: "large",
                            },
                        ],
                        Forms.Radiobutton.bind(
                            this,
                            "size",
                            undefined,
                            "medium"
                        )
                    ),
                ],
            },
            activated: isString(this.size),
        });

        this.editor.groups.options();
        this.editor.required(this);
        this.editor.visibility();

        const defaultLabelForTrue = pgettext(
            "block:picture-choice",
            "Selected"
        );
        const defaultLabelForFalse = pgettext(
            "block:picture-choice",
            "Not selected"
        );

        const labelsFeature = this.editor.option({
            name: pgettext("block:picture-choice", "Labels"),
            form: {
                title: pgettext("block:picture-choice", "Labels"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForTrue", undefined)
                    ).placeholder(defaultLabelForTrue),
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForFalse", undefined)
                    ).placeholder(defaultLabelForFalse),
                    new Forms.Static(
                        pgettext(
                            "block:picture-choice",
                            "These labels will be used in the dataset and override the default values %1 and %2.",
                            `**${defaultLabelForTrue}**`,
                            `**${defaultLabelForFalse}**`
                        )
                    ).markdown(),
                ],
            },
            activated:
                isString(this.labelForTrue) || isString(this.labelForFalse),
            disabled: !this.multiple,
        });

        this.editor.scores({
            target: this,
            collection,
            description: pgettext(
                "block:picture-choice",
                "Generates a score based on the selected choices. Open the settings panel for each choice to set the individual score for that choice."
            ),
        });

        this.editor.alias(this);

        const formatSeparatorCustom = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "formatSeparatorCustom", undefined)
        )
            .visible(this.formatSeparator === "custom")
            .sanitize(false)
            .width(200)
            .label(pgettext("block:picture-choice", "Use this separator:"));

        const formatSeparatorOptions = new Forms.Group([
            new Forms.Dropdown<
                | "comma"
                | "space"
                | "list"
                | "bullets"
                | "numbers"
                | "conjunction"
                | "disjunction"
                | "custom"
            >(
                [
                    {
                        label: pgettext(
                            "block:picture-choice",
                            "Comma separated"
                        ),
                        value: "comma",
                    },
                    {
                        label: pgettext(
                            "block:picture-choice",
                            "Space separated"
                        ),
                        value: "space",
                    },
                    {
                        label: pgettext(
                            "block:picture-choice",
                            "List on multiple lines"
                        ),
                        value: "list",
                    },
                    {
                        label: pgettext(
                            "block:picture-choice",
                            "Bulleted list"
                        ),
                        value: "bullets",
                    },
                    {
                        label: pgettext(
                            "block:picture-choice",
                            "Numbered list"
                        ),
                        value: "numbers",
                    },
                    {
                        label: pgettext(
                            "block:picture-choice",
                            "Language sensitive conjunction (_, _, and _)"
                        ),
                        value: "conjunction",
                    },
                    {
                        label: pgettext(
                            "block:picture-choice",
                            "Language sensitive disjunction (_, _, or _)"
                        ),
                        value: "disjunction",
                    },
                    {
                        label: pgettext(
                            "block:picture-choice",
                            "Custom separator"
                        ),
                        value: "custom",
                    },
                ],
                Forms.Radiobutton.bind(
                    this,
                    "formatSeparator",
                    undefined,
                    "comma"
                )
            )
                .label(
                    pgettext(
                        "block:picture-choice",
                        "How to separate the selected images:"
                    )
                )
                .on((formatSeparator) => {
                    formatSeparatorCustom.visible(
                        formatSeparator.value === "custom"
                    );
                }),
            formatSeparatorCustom,
        ]).visible(this.format === "concatenate" || this.format === "both");

        const formatFeature = this.editor.option({
            name: pgettext("block:picture-choice", "Data format"),
            form: {
                title: pgettext("block:picture-choice", "Data format"),
                controls: [
                    new Forms.Radiobutton<"fields" | "concatenate" | "both">(
                        [
                            {
                                label: pgettext(
                                    "block:picture-choice",
                                    "Every image as a separate field"
                                ),
                                description: pgettext(
                                    "block:picture-choice",
                                    "Every image is included in the dataset as a separate value."
                                ),
                                value: "fields",
                            },
                            {
                                label: pgettext(
                                    "block:picture-choice",
                                    "Text field with a list of all selected images"
                                ),
                                description: pgettext(
                                    "block:picture-choice",
                                    "All the selected images are concatenated to a single string of text separated using a configurable separator."
                                ),
                                value: "concatenate",
                            },
                            {
                                label: pgettext(
                                    "block:picture-choice",
                                    "Both options above"
                                ),
                                description: pgettext(
                                    "block:picture-choice",
                                    "Includes every image in the dataset together with the concatenated text."
                                ),
                                value: "both",
                            },
                        ],
                        Forms.Radiobutton.bind(
                            this,
                            "format",
                            undefined,
                            "fields"
                        )
                    )
                        .label(
                            pgettext(
                                "block:picture-choice",
                                "This setting determines how the data is stored in the dataset:"
                            )
                        )
                        .on((format) => {
                            formatSeparatorOptions.visible(
                                format.value === "concatenate" ||
                                    format.value === "both"
                            );
                        }),
                    formatSeparatorOptions,
                ],
            },
            activated: isString(this.format),
            disabled: !this.multiple,
        });

        this.editor.exportable(this);
    }

    @conditions
    defineConditions(): void {
        this.choices.each((choice: Choice) => {
            if (choice.name && !isString(choice.url)) {
                this.conditions.template({
                    condition: PictureChoiceCondition,
                    label: choice.name,
                    icon: this.multiple ? ICON_MULTIPLE : ICON_SINGLE,
                    burst: "branch",
                    props: {
                        choice: choice,
                        slot: this.slots.select(
                            this.multiple ? choice.id : "choice"
                        ),
                    },
                });
            }
        });

        if (this.choices.count > 0) {
            this.conditions.template({
                condition: PictureChoiceUndefinedCondition,
                separator: true,
            });
        }

        const counter = this.slots.select("counter", "feature");

        if (counter && counter.label) {
            const group = this.conditions.group(counter.label, ICON_COUNTER);

            each(
                [
                    {
                        mode: "equal",
                        label: pgettext(
                            "block:picture-choice",
                            "Counter is equal to"
                        ),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:picture-choice",
                            "Counter is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext(
                            "block:picture-choice",
                            "Counter is lower than"
                        ),
                    },
                    {
                        mode: "above",
                        label: pgettext(
                            "block:picture-choice",
                            "Counter is higher than"
                        ),
                    },
                    {
                        mode: "between",
                        label: pgettext(
                            "block:picture-choice",
                            "Counter is between"
                        ),
                    },
                    {
                        mode: "not-between",
                        label: pgettext(
                            "block:picture-choice",
                            "Counter is not between"
                        ),
                    },
                ],
                (condition: { mode: TCounterModes; label: string }) => {
                    group.template({
                        condition: CounterCondition,
                        label: condition.label,
                        autoOpen: true,
                        props: {
                            slot: counter,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );
        }

        const score = this.slots.select("score", "feature");

        if (score && score.label) {
            const group = this.conditions.group(score.label, ICON_SCORE);

            each(
                [
                    {
                        mode: "equal",
                        label: pgettext(
                            "block:picture-choice",
                            "Score is equal to"
                        ),
                    },
                    {
                        mode: "not-equal",
                        label: pgettext(
                            "block:picture-choice",
                            "Score is not equal to"
                        ),
                    },
                    {
                        mode: "below",
                        label: pgettext(
                            "block:picture-choice",
                            "Score is lower than"
                        ),
                    },
                    {
                        mode: "above",
                        label: pgettext(
                            "block:picture-choice",
                            "Score is higher than"
                        ),
                    },
                    {
                        mode: "between",
                        label: pgettext(
                            "block:picture-choice",
                            "Score is between"
                        ),
                    },
                    {
                        mode: "not-between",
                        label: pgettext(
                            "block:picture-choice",
                            "Score is not between"
                        ),
                    },
                    {
                        mode: "defined",
                        label: pgettext(
                            "block:picture-choice",
                            "Score is calculated"
                        ),
                    },
                    {
                        mode: "undefined",
                        label: pgettext(
                            "block:picture-choice",
                            "Score is not calculated"
                        ),
                    },
                ],
                (condition: { mode: TScoreModes; label: string }) => {
                    group.template({
                        condition: ScoreCondition,
                        label: condition.label,
                        autoOpen:
                            condition.mode !== "defined" &&
                            condition.mode !== "undefined",
                        props: {
                            slot: score,
                            mode: condition.mode,
                            value: 0,
                            to:
                                condition.mode === "between" ||
                                condition.mode === "not-between"
                                    ? 0
                                    : undefined,
                        },
                    });
                }
            );
        }
    }
}
