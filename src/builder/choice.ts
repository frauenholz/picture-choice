/** Dependencies */
import {
    Collection,
    Components,
    Forms,
    Markdown,
    REGEX_IS_URL,
    Slots,
    affects,
    alias,
    created,
    definition,
    deleted,
    editor,
    insertVariable,
    isBoolean,
    isString,
    markdownifyToString,
    name,
    pgettext,
    refreshed,
    renamed,
    reordered,
    score,
} from "tripetto";
import { PictureChoice } from "./";

export class Choice extends Collection.Item<PictureChoice> {
    @definition("string")
    @name
    name = "";

    @definition("boolean", "optional")
    nameVisible = true;

    @definition("string", "optional")
    image?: string;

    @definition("string", "optional")
    emoji?: string;

    @definition("string", "optional")
    description?: string;

    @definition("string", "optional")
    @affects("#refresh")
    url?: string;

    @definition("string", "optional")
    target?: "self" | "blank";

    @definition("string", "optional")
    @affects("#refresh")
    moniker?: string;

    @definition("string", "optional")
    @alias
    value?: string;

    @definition("string", "optional")
    @affects("#refresh")
    labelForTrue?: string;

    @definition("string", "optional")
    @affects("#refresh")
    labelForFalse?: string;

    @definition("number", "optional")
    @score
    score?: number;

    @definition("boolean", "optional")
    @affects("#name")
    exclusive?: boolean;

    @created
    @reordered
    @renamed
    @refreshed
    defineSlot(): void {
        if (this.ref.multiple && !isString(this.url)) {
            const choiceName =
                (this.name &&
                    markdownifyToString(
                        this.name,
                        Markdown.MarkdownFeatures.None
                    )) ||
                undefined;

            const slot = this.ref.slots.dynamic({
                type: Slots.Boolean,
                reference: this.id,
                label: pgettext("block:picture-choice", "Image"),
                sequence: this.index,
                name: choiceName,
                alias: this.value,
                required: this.ref.required,
                exportable:
                    this.ref.format !== "concatenate" && this.ref.exportable,
                pipeable: {
                    label: pgettext("block:picture-choice", "Image"),
                    content: this.moniker
                        ? {
                              string: choiceName || "",
                              markdown: this.moniker,
                          }
                        : this.name !== choiceName
                        ? {
                              string: choiceName || "",
                              text: this.name,
                          }
                        : "name",
                    alias: this.ref.alias,
                    legacy: "Image",
                },
            });

            slot.labelForTrue =
                this.labelForTrue ||
                this.ref.labelForTrue ||
                pgettext("block:picture-choice", "Selected");

            slot.labelForFalse =
                this.labelForFalse ||
                this.ref.labelForFalse ||
                pgettext("block:picture-choice", "Not selected");
        } else {
            this.deleteSlot();
        }
    }

    @deleted
    deleteSlot(): void {
        this.ref.slots.delete(this.id, "dynamic");
    }

    @editor
    defineEditor(): void {
        const image = new Forms.Text(
            "singleline",
            Forms.Text.bind(this, "image", undefined)
        )
            .label(pgettext("block:picture-choice", "Image source URL"))
            .inputMode("url")
            .placeholder("https://")
            .action("@", insertVariable(this))
            .autoValidate((ref: Forms.Text) =>
                ref.value === ""
                    ? "unknown"
                    : REGEX_IS_URL.test(ref.value) ||
                      (ref.value.length > 23 &&
                          ref.value.indexOf("data:image/jpeg;base64,") === 0) ||
                      (ref.value.length > 22 &&
                          ref.value.indexOf("data:image/png;base64,") === 0) ||
                      (ref.value.length > 22 &&
                          ref.value.indexOf("data:image/svg;base64,") === 0) ||
                      (ref.value.length > 22 &&
                          ref.value.indexOf("data:image/gif;base64,") === 0) ||
                      (ref.value.length > 1 && ref.value.charAt(0) === "/")
                    ? "pass"
                    : "fail"
            )
            .visible(!isString(this.emoji));

        const emoji = new Forms.Group([
            new Forms.Text(
                "singleline",
                Forms.Text.bind(this, "emoji", undefined)
            )
                .label(pgettext("block:picture-choice", "Emoji"))
                .maxLength(12)
                .width(65)
                .align("center"),
            new Forms.Static(
                pgettext(
                    "block:picture-choice",
                    "Windows users press: **WIN + .**"
                )
            ).markdown(),
            new Forms.Static(
                pgettext(
                    "block:picture-choice",
                    "MacOS users press: **CTRL + CMD + Space**"
                )
            ).markdown(),
        ]).visible(isString(this.emoji));

        this.editor.option({
            name: pgettext("block:picture-choice", "Name"),
            form: {
                title: pgettext("block:picture-choice", "Image name"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "name", "")
                    )
                        .action("@", insertVariable(this))
                        .autoFocus()
                        .autoSelect(),
                    new Forms.Checkbox(
                        pgettext(
                            "block:picture-choice",
                            "Show this name with the image"
                        ),
                        Forms.Checkbox.bind(this, "nameVisible", true)
                    ),
                ],
            },
            locked: true,
        });

        this.editor.form({
            title: pgettext("block:picture-choice", "Image"),
            controls: [
                new Forms.Radiobutton<"image" | "emoji">(
                    [
                        {
                            label: pgettext(
                                "block:picture-choice",
                                "Use an image"
                            ),
                            value: "image",
                        },
                        {
                            label: pgettext(
                                "block:picture-choice",
                                "Use an Emoji"
                            ),
                            value: "emoji",
                        },
                    ],
                    isString(this.emoji) ? "emoji" : "image"
                ).on((type) => {
                    this.emoji =
                        type.value === "emoji" ? this.emoji || "" : undefined;

                    image.visible(type.value === "image");
                    emoji.visible(type.value === "emoji");
                }),
                image,
                emoji,
            ],
        });

        this.editor.option({
            name: pgettext("block:picture-choice", "Description"),
            form: {
                title: pgettext("block:picture-choice", "Image description"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "description", undefined)
                    ).action("@", insertVariable(this)),
                ],
            },
            activated: isString(this.description),
        });

        this.editor.group(pgettext("block:picture-choice", "Options"));

        this.editor.option({
            name: pgettext("block:picture-choice", "URL"),
            form: {
                title: pgettext("block:picture-choice", "URL"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "url", undefined)
                    )
                        .placeholder("https://")
                        .action("@", insertVariable(this))
                        .autoValidate((url: Forms.Text) =>
                            url.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(url.value)
                                ? "pass"
                                : "fail"
                        ),
                    new Forms.Checkbox(
                        pgettext(
                            "block:picture-choice",
                            "Open in new tab/window"
                        ),
                        this.target !== "self"
                    ).on((target) => {
                        this.target = target.isFeatureEnabled
                            ? target.isChecked
                                ? "blank"
                                : "self"
                            : undefined;
                    }),
                    new Forms.Static(
                        pgettext(
                            "block:picture-choice",
                            // tslint:disable-next-line:max-line-length
                            "If a URL is set, clicking the image will open it. The image cannot be selected as answer."
                        )
                    ),
                ],
            },
            activated: isString(this.url),
            on: (urlFeature: Components.Feature<Forms.Form>) => {
                monikerFeature.disabled(urlFeature.isActivated);
                identifierFeature.disabled(urlFeature.isActivated);
                exclusivityFeature.disabled(
                    urlFeature.isActivated || !this.ref.multiple
                );
                labelsFeature.disabled(
                    urlFeature.isActivated || !this.ref.multiple
                );
            },
        });

        const monikerFeature = this.editor.option({
            name: pgettext("block:picture-choice", "Moniker"),
            form: {
                title: pgettext("block:picture-choice", "Image moniker"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "moniker", undefined)
                    ).action("@", insertVariable(this)),
                    new Forms.Static(
                        pgettext(
                            "block:picture-choice",
                            "If a moniker is set, this moniker will be used when referred to this image."
                        )
                    ),
                ],
            },
            activated: isString(this.moniker),
            disabled: isString(this.url),
        });

        const exclusivityFeature = this.editor.option({
            name: pgettext("block:picture-choice", "Exclusivity"),
            form: {
                title: pgettext("block:picture-choice", "Image exclusivity"),
                controls: [
                    new Forms.Checkbox(
                        pgettext(
                            "block:picture-choice",
                            "Unselect all other selected images when selected"
                        ),
                        Forms.Checkbox.bind(this, "exclusive", undefined, true)
                    ),
                ],
            },
            activated:
                (this.ref.multiple &&
                    !isString(this.url) &&
                    isBoolean(this.exclusive)) ||
                false,
            disabled: !this.ref.multiple || isString(this.url),
        });

        const defaultLabelForTrue = pgettext(
            "block:picture-choice",
            "Selected"
        );
        const defaultLabelForFalse = pgettext(
            "block:picture-choice",
            "Not selected"
        );

        const labelsFeature = this.editor.option({
            name: pgettext("block:picture-choice", "Labels"),
            form: {
                title: pgettext("block:picture-choice", "Labels"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForTrue", undefined)
                    ).placeholder(defaultLabelForTrue),
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "labelForFalse", undefined)
                    ).placeholder(defaultLabelForFalse),
                    new Forms.Static(
                        pgettext(
                            "block:picture-choice",
                            "These labels will be used in the dataset and override the default values %1 and %2.",
                            `**${defaultLabelForTrue}**`,
                            `**${defaultLabelForFalse}**`
                        )
                    ).markdown(),
                ],
            },
            activated:
                isString(this.labelForTrue) || isString(this.labelForFalse),
            disabled: !this.ref.multiple || isString(this.url),
        });

        const identifierFeature = this.editor.option({
            name: pgettext("block:picture-choice", "Identifier"),
            form: {
                title: pgettext("block:picture-choice", "Image identifier"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "value", undefined)
                    ),
                    new Forms.Static(
                        pgettext(
                            "block:picture-choice",
                            "If an image identifier is set, this identifier will be used instead of the label."
                        )
                    ),
                ],
            },
            activated: isString(this.value),
            disabled: isString(this.url),
        });

        const scoreSlot = this.ref.slots.select<Slots.Numeric>(
            "score",
            "feature"
        );

        this.editor.option({
            name: pgettext("block:picture-choice", "Score"),
            form: {
                title: pgettext("block:picture-choice", "Score"),
                controls: [
                    new Forms.Numeric(
                        Forms.Numeric.bind(this, "score", undefined)
                    )
                        .precision(scoreSlot?.precision || 0)
                        .digits(scoreSlot?.digits || 0)
                        .decimalSign(scoreSlot?.decimal || "")
                        .thousands(
                            scoreSlot?.separator ? true : false,
                            scoreSlot?.separator || ""
                        )
                        .prefix(scoreSlot?.prefix || "")
                        .prefixPlural(scoreSlot?.prefixPlural || undefined)
                        .suffix(scoreSlot?.suffix || "")
                        .suffixPlural(scoreSlot?.suffixPlural || undefined),
                ],
            },
            activated: true,
            locked: scoreSlot ? true : false,
            disabled: scoreSlot ? false : true,
        });
    }
}
